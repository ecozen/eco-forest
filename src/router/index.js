import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: "/PEsriMap",
        name: "PEsriMap",
        component: () =>
            import("../components/arcgismap/EsriMap.vue"),
    },
    {
        path: "/FeatureCluster",
        name: "FeatureCluster",
        component: () =>
            import( "../components/arcgismap/FeatureCluster.vue"),
    },
    {
        path: "/TdtWebTileLayer",
        name: "TdtWebTileLayer",
        component: () =>
            import("../components/arcgismap/TdtWebTileLayer.vue"),
    },
    {
        path: "/TdtWMTSLayer",
        name: "TdtWMTSLayer",
        component: () =>
            import("../components/arcgismap/TdtWMTSLayer.vue"),
    },
    {
        path: "/ForeststarMap",
        name: "ForeststarMap",
        component: () =>
            import("../components/arcgismap/ForeststarMap.vue"),
    },
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: () => import('../views/Home'),
        children: [          
            {
                path: "",
                name: "PMapView",
                component: () => import("../components/arcgismap/MapView.vue"),
                meta: {title: '~~~'}
            },
            {
              path: 'PEsriMap',
              name: 'PEsriMap',
              component: () => import('../components/arcgismap/EsriMap.vue'),
              meta: {title: '三维地图'}
          },
          {
              path: 'featurecluster',
              name: 'PFeatureCluster',
              component: () => import('../components/arcgismap/FeatureCluster.vue'),
              meta: {title: '聚合'}
          }
        ]
    },
    {
        path: '/404',
        component: () => import('../views/404'),
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
