const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ArcGISPlugin = require("@arcgis/webpack-plugin");
// const DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = (env = {}) => ({
  mode: env.prod ? 'production' : 'development',
  devtool: env.prod ? 'source-map' : 'eval-cheap-module-source-map',
  entry:path.resolve(__dirname, './src/main.js'),

  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: ""
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          transpileOnly: true
        }
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.(jpe?g|png|gif|webp)$/,
        use: {
          loader: 'url-loader',
          options: { limit: 8192 }
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: false }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: { hmr: !env.prod }
          },
          'css-loader'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          "sass-loader",
          // {
          //   loader: "resolve-url-loader",
          //   options: { includeRoot: true }
          // },
          // "sass-loader?sourceMap"
        ],
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new ArcGISPlugin({
      // disable provided asset loaders
      useDefaultAssetLoaders: false
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: 'true',
      __VUE_PROD_DEVTOOLS__: 'false'
    })
  ],
  node: {
    process: false,
    global: false,
    fs: 'empty',
  },
  devServer: {
    inline: true,
    hot: true,
    open:true,
    stats: 'minimal',
    contentBase: __dirname,
    overlay: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
  }
  }
})
